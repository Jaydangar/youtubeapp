package android.example.com.playvideos;

public class VideoDetails {
    String VideoTitle;
    String URL;
    String VideoId;

    public void setVideoTitle(String VideoTitle) {
        this.VideoTitle = VideoTitle;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }

    public void setVideoId(String VideoId) {
        this.VideoId = VideoId;
    }

    public String getVideoId() {
        return VideoId;
    }
}