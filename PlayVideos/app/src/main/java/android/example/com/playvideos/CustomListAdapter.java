package android.example.com.playvideos;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import douglasspgyn.com.github.doubletapview.DoubleTapView;

public class CustomListAdapter extends BaseAdapter {
    Activity activity;

    private LayoutInflater inflater;
    ArrayList<VideoDetails> singletons;

    public CustomListAdapter(Activity activity, ArrayList<VideoDetails> singletons) {
        this.activity = activity;
        this.singletons = singletons;
    }
    public int getCount() {
        return this.singletons.size();
    }

    public Object getItem(int i) {
        return this.singletons.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (this.inflater == null) {
            this.inflater = (LayoutInflater) this.activity.getLayoutInflater();
            // getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.videolist, null);
        }

        DoubleTapView networkImageView = convertView.findViewById(R.id.doubleTapView);
        final TextView imgtitle = (TextView) convertView.findViewById(R.id.video_title);
        final TextView tvURL=(TextView)convertView.findViewById(R.id.tv_url);
        final TextView tvVideoID=(TextView)convertView.findViewById(R.id.tv_videoId);
        ((LinearLayout) convertView.findViewById(R.id.asser)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(), VideoActivity.class);
                intent.putExtra("videoId",tvVideoID.getText().toString());
                view.getContext().startActivity(intent);
            }
        });

        VideoDetails singleton = this.singletons.get(i);

        //  Via Glide Setting up the Images...
        Glide.with(convertView.getContext()).load(singleton.getURL()).into(networkImageView.getBackgroundImageView());

        tvVideoID.setText(singleton.getVideoId());
        imgtitle.setText(singleton.getVideoTitle());
        return convertView;
    }
}