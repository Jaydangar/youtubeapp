package android.example.com.playvideos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.PeopleServiceScopes;
import com.google.api.services.people.v1.model.Address;
import com.google.api.services.people.v1.model.Birthday;
import com.google.api.services.people.v1.model.EmailAddress;
import com.google.api.services.people.v1.model.Gender;
import com.google.api.services.people.v1.model.Name;
import com.google.api.services.people.v1.model.Person;
import com.google.api.services.people.v1.model.Photo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    protected static GoogleApiClient mGoogleApiClient;

    //  Creating Request Code
    private int RC_SIGN_IN = 101;
    private int RC_API_CHECK = 102;

    //  Used for SharedPreferences File Name
    private static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        //  GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (mGoogleApiClient.isConnected()) {
            Intent feedIntent = new Intent(SignInActivity.this, Feeds.class);
            startActivity(feedIntent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                // The serverClientId is an OAuth 2.0 web client ID
                .requestServerAuthCode(getString(R.string.ClientID))
                .requestEmail().requestScopes(new Scope(PeopleServiceScopes.PLUS_LOGIN),
                        new Scope(PeopleServiceScopes.USERINFO_PROFILE))
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn() {
        // Shows an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //  After the user signs in, you can get a GoogleSignInAccount object for the user in the activity's onActivityResult method.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        switch (requestCode) {
            case 101:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                if (result.isSuccess()) {
                    GoogleSignInAccount acct = result.getSignInAccount();
                    Log.d("Token_Success...", "onActivityResult:GET_TOKEN:success:" + result.getStatus().isSuccess());
                    String ServerCode = null;
                    if (acct != null) {
                        ServerCode = acct.getServerAuthCode();
                        Log.d("ServerAuth", "ServerAuth : " + ServerCode);
                        // This is what we need to exchange with the server.
                        try {
                            List<String> getDetails = new PeoplesAsync().execute(ServerCode).get();

                            // For Caching purpose, I have used SharedPreferences...
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("Name", getDetails.get(0));
                            editor.putString("Email", getDetails.get(1));
                            editor.putString("Age", getDetails.get(2));
                            editor.putString("ProfileURL", getDetails.get(3));
                            editor.putString("Gender", getDetails.get(4));
                            editor.putString("Address", getDetails.get(5));
                            editor.putString("BDay", getDetails.get(6));
                            editor.apply();

                            Intent FeedsIntent = new Intent(SignInActivity.this, Feeds.class);
                            startActivity(FeedsIntent);
                            finish();

                            getDetails.clear();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    Log.d("TokenFailed...", result.getStatus().toString() + "\nmsg: " + result.getStatus().getStatusMessage());
                }
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    class PeoplesAsync extends AsyncTask<String, Void, List<String>> {

        @Override
        protected List<String> doInBackground(String... params) {

            List<String> nameList = new ArrayList<>();

            try {

                PeopleService peopleService = setUp(SignInActivity.this, params[0]);

                Person profile = peopleService.people().get("people/me").setPersonFields("names,emailAddresses,photos,genders,birthdays,ageRanges,addresses").execute();

                if (!profile.isEmpty()) {

                    List<Name> names = profile.getNames();
                    List<EmailAddress> emailAddresses = profile.getEmailAddresses();
                    List<Photo> profileImages = profile.getPhotos();
                    List<Gender> genderList = profile.getGenders();
                    List<Address> Addresses = profile.getAddresses();
                    List<Birthday> BirthDays = profile.getBirthdays();

                    String Age = profile.getAgeRange();

                    if (!nameList.isEmpty()) {
                        nameList.clear();
                    } else {

                        if (names != null) {
                            String displayName = names.get(0).getDisplayName();
                            nameList.add(displayName);
                            Log.d("Name", "Name : " + displayName);
                        } else {
                            nameList.add("");
                        }

                        if (emailAddresses != null) {
                            String email = emailAddresses.get(0).getValue();
                            nameList.add(email);
                            Log.d("Name", "Email : " + email);
                        } else {
                            nameList.add("");
                        }

                        if (Age != null) {
                            Log.d("Name", "AgeRange: " + Age);
                            nameList.add(Age);
                        } else {
                            nameList.add("");
                        }


                        if (profileImages != null) {
                            String profileImage = profileImages.get(0).getUrl();
                            nameList.add(profileImage);
                            Log.d("Name", "profileUrl : " + profileImage);
                        } else {
                            nameList.add("");
                        }

                        if (genderList != null) {
                            String Gender = genderList.get(0).getValue();
                            nameList.add(Gender);
                            Log.d("Name", "Gender : " + Gender);
                        } else {
                            nameList.add("");
                        }

                        if (Addresses != null) {
                            String Address = Addresses.get(0).getStreetAddress();
                            nameList.add(Address);
                            Log.d("Name", "Address : " + Address);
                        } else {
                            nameList.add("");
                        }

                        if (BirthDays != null) {
                            String BirthDay = BirthDays.get(0).getText();
                            nameList.add(BirthDay);
                            Log.d("Name", "BirthDay : " + BirthDay);
                        } else {
                            nameList.add("");
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return nameList;
        }
    }

    public static PeopleService setUp(Context context, String serverAuthCode) throws IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        // Redirect URL for web based applications.
        // Can be empty too.
        String redirectUrl = "urn:ietf:wg:oauth:2.0:oob";

        // STEP 1
        GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
                httpTransport,
                jsonFactory,
                context.getString(R.string.ClientID),
                context.getString(R.string.Secret),
                serverAuthCode,
                redirectUrl).execute();

        // STEP 2
        GoogleCredential credential = new GoogleCredential.Builder()
                .setClientSecrets(context.getString(R.string.ClientID), context.getString(R.string.Secret))
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .build();

        credential.setFromTokenResponse(tokenResponse);

        // STEP 3
        return new PeopleService.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName("PlayVideos")
                .build();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }
}