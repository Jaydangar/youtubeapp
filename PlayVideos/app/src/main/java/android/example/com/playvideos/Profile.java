package android.example.com.playvideos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;

import static android.example.com.playvideos.SignInActivity.mGoogleApiClient;

public class Profile extends AppCompatActivity implements View.OnClickListener {

    //  Used for SharedPreferences File Name
    private static final String MY_PREFS_NAME = "MyPrefsFile";

    private ImageView header_cover_image, user_profile_photo;
    private TextView user_profile_name, EmailTV, AgeTV, GenderTV, AddressTV, BirthDayTV;
    private Button SignOutBtn;

    private String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //  ImageViews
        header_cover_image = findViewById(R.id.header_cover_image);
        user_profile_photo = findViewById(R.id.user_profile_photo);

        //  TextVies
        user_profile_name = findViewById(R.id.user_profile_name);
        EmailTV = findViewById(R.id.Email);
        AgeTV = findViewById(R.id.Age);
        GenderTV = findViewById(R.id.Gender);
        AddressTV = findViewById(R.id.Address);
        BirthDayTV = findViewById(R.id.BirthDay);

        //  Button
        SignOutBtn = findViewById(R.id.SignOutBtn);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        String Name = prefs.getString("Name", null);
        String Email = prefs.getString("Email", null);
        String Age = prefs.getString("Age", null);
        String ProfileImage = prefs.getString("ProfileURL", null);
        String Gender = prefs.getString("Gender", null);
        String Address = prefs.getString("Address", null);
        String BirthDay = prefs.getString("BDay", null);

        if (Name != null) {
            Log.d(TAG, Name);
            user_profile_name.setText(Name);
        }

        if (Email != null) {
            Log.d(TAG, Email);
            EmailTV.setText(Email);
        }

        if (Age != null) {
            Log.d(TAG, Age);
            AgeTV.setText(Age);
        }

        if (ProfileImage != null) {
            Log.d(TAG, ProfileImage);
            //  Via Glide Setting up the Images...
            Glide.with(this).load(ProfileImage).into(user_profile_photo);
        }

        if (Gender != null) {
            GenderTV.setText(Gender);
            Log.d(TAG, Gender);
        }

        if (Address != null) {
            Log.d(TAG, Address);
            AddressTV.setText(Address);
        }

        if (BirthDay != null) {
            Log.d(TAG, BirthDay);
            BirthDayTV.setText(BirthDay);
        }

        SignOutBtn.setOnClickListener(this);
    }

    //This code clears which account is connected to the app. To sign in again, the user must choose their account again.
    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onClick(View v) {
        signOut();
        Intent ExitIntent = new Intent(v.getContext(), SignInActivity.class);
        startActivity(ExitIntent);
        finish();
    }
}
