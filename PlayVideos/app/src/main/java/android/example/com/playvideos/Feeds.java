package android.example.com.playvideos;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Feeds extends AppCompatActivity {

    //  Developer Key
    private static final String API_Key = "AIzaSyDGs5ssJ4nkOW7cFri5KvE-r0BKNLhYYOo";

    /**
     * Define a global instance of a Youtube object, which will be used
     * to make YouTube Data API requests.
     */
    private static YouTube youtube;

    private ListView listView;
    private ArrayList<VideoDetails> videoDetailsArrayList;;
    private CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);

        listView = findViewById(R.id.videoList);

        videoDetailsArrayList = new ArrayList<>();
        customListAdapter=new CustomListAdapter(Feeds.this,videoDetailsArrayList);

        new BackGroundTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profileoptions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_name) {
            Intent profileIntent = new Intent(Feeds.this,Profile.class);
            startActivity(profileIntent);
        }

        return super.onOptionsItemSelected(item);
    }


    class BackGroundTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {
                    Log.d("Initialize...", "initialize: Initialization Completed...");
                }
            }).setApplicationName("youtubeApiDemo").build();

            try {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("API_Key", API_Key);
                parameters.put("part", "snippet");
                parameters.put("maxResults", "50");
                parameters.put("q", "Comedy");
                parameters.put("type", "Comedy");

                YouTube.Search.List searchListByKeywordRequest = youtube.search().list(parameters.get("part"));
                searchListByKeywordRequest.setKey(API_Key);

                if (parameters.containsKey("maxResults")) {
                    searchListByKeywordRequest.setMaxResults(Long.parseLong(parameters.get("maxResults")));
                }

                if (parameters.containsKey("q") && !parameters.get("q").equals("")) {
                    searchListByKeywordRequest.setQ(parameters.get("q"));
                }

                if (parameters.containsKey("type") && !parameters.get("type").equals("")) {
                    searchListByKeywordRequest.setType(parameters.get("type"));
                }

                SearchListResponse response = searchListByKeywordRequest.execute();

                try {
                    JSONObject root = new JSONObject(response);
                    JSONArray items = root.getJSONArray("items");

                    for (int i = 0; i < items.length(); i++) {

                        JSONObject parent = items.getJSONObject(i);
                        JSONObject id = parent.getJSONObject("id");

                        String videoID = id.getString("videoId");

                        JSONObject snippets = parent.getJSONObject("snippet");
                        JSONObject Thumbnail = snippets.getJSONObject("thumbnails");
                        JSONObject ThumbnailDefault = Thumbnail.getJSONObject("default");

                        String ThumbnailURL = ThumbnailDefault.getString("url");

                        String Title = snippets.getString("title");

                        VideoDetails videoDetails=new VideoDetails();

                        videoDetails.setURL(ThumbnailURL);
                        videoDetails.setVideoTitle(Title);
                        videoDetails.setVideoId(videoID);

                        videoDetailsArrayList.add(videoDetails);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(customListAdapter);
                            customListAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
